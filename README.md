# kernl(lab)

> A showcase for the kernl(ui) frontend framework.

## Build Setup

``` bash
# install dependencies
$ yarn install

# build for production and launch server
$ yarn run build
$ yarn start

# serve with hot reload at localhost:3000
$ yarn run dev

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
