import { ALL_YEARS, ALL_COLLEGES } from '../utils/utils';
import { getAllData } from "../utils/queries.js"
import API from "../utils/configs.js"

export const state = () => ({
    years: [],
    colleges: [],
    majors: [],
    dump: [],
    contrast: false,
    activeYear: ALL_YEARS,
    activeCollege: ALL_COLLEGES,
    activeMajors: []
})

export const mutations = {
    setDump(state, dump) {
        state.dump = dump;
    },

    setYears(state, years) {
        state.years = years
    },

    setMajors(state, majors) {
        state.majors = majors
    },

    setColleges(state, colleges) {
        state.colleges = colleges
    },

    resetFilters(state) {
        state.activeYear = ALL_YEARS
        state.activeCollege = ALL_COLLEGES
        state.activeMajors = []
    },

    setActiveYear(state, year) {
        state.activeYear = year
    },

    setActiveCollege(state, college) {
        state.activeCollege = college
    },

    setActiveMajors(state, majors) {
        state.activeMajors = majors
    },

    setContrast(state, contrast) {
        state.contrast = contrast
    }
}

export const getters =  {
    areFiltersApplied: (state) => () => {
        return state.activeMajors.length > 0 
            || state.activeYear != ALL_YEARS 
            || state.activeCollege != ALL_COLLEGES;
    },
    filteredData: (state) => () => {
        return state.dump.filter(element =>
            (state.activeYear == ALL_YEARS || element.job_year == state.activeYear) &&
            (state.activeCollege == ALL_COLLEGES || element.collegedesc == state.activeCollege) &&
            (state.activeMajors.length == 0 || state.activeMajors.includes(element.majordesc))
        )
    }
}

export const actions = {
    async nuxtServerInit({ commit, state }) {
       // Maybe put middleware here?
    }
}