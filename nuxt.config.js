import pkg from './package'

const webpack = require('webpack')

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources'
  ],

  /*
  ** Global CSS
  */
  css: [
    '~assets/styles/main.scss'
  ],

  styleResources: {
    scss: [
      'kernl-ui/dist/styles/main.css',
      'formstone/dist/css/navigation.css',
      'vue-multiselect/dist/vue-multiselect.min.css',
      'kernl-ui/src/styles/01.global/variables/colors.scss',
      'kernl-ui/src/styles/01.global/variables/icons.scss',
      'kernl-ui/src/styles/01.global/variables/layout.scss',
      'kernl-ui/src/styles/01.global/variables/misc.scss',
      'kernl-ui/src/styles/01.global/variables/sizing.scss',
      'kernl-ui/src/styles/01.global/variables/typography.scss',
      'kernl-ui/src/styles/01.global/methods/colors.scss',
      'kernl-ui/src/styles/01.global/methods/layout.scss',
      'kernl-ui/src/styles/01.global/methods/media-queries.scss',
      'kernl-ui/src/styles/01.global/methods/misc.scss',
      'kernl-ui/src/styles/01.global/methods/sizing.scss',
      'kernl-ui/src/styles/01.global/methods/typography.scss',
      'kernl-ui/src/styles/04.components/_variables.scss',
      'kernl-ui/src/styles/04.components/_methods.scss'
    ]
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // '~/plugins/plugins.js',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
