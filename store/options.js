import kernl from '../assets/data/utilities'

export const state = () => ({
  badge: {
    scoped: [],
    utils: [
      { 'Background Color': kernl.utilities.background },
      { 'Text Color': kernl.utilities.color },
      { 'Border Colors': kernl.utilities.border }
    ]
  },
  button: {
    scoped: [
      {
        type: 'select',
        label: 'Small',
        value: '--sm'
      }
    ],
    utils: [
      { 'Background Color': kernl.utilities.background },
      { 'Text Color': kernl.utilities.color },
      { 'Border Colors': kernl.utilities.border }
    ]
  },
  card: {
    scoped: [
      {
        type: 'bool',
        label: 'Has Image',
        value: 'has-image'
      },
      {
        type: 'bool',
        label: 'Horizontal',
        value: '--h'
      },
      {
        type: 'bool',
        label: 'Overlay',
        value: '--overlay'
      }
    ],
    utils: [
      { 'Background Color': kernl.utilities.background },
      { 'Font Size': kernl.utilities.fontSize },
      { 'Border Colors': kernl.utilities.border }
    ]
  },
  listGroup: {
    scoped: [
      {
        type: 'select',
        label: 'Indent on Hover',
        value: '--indent'
      },
      {
        type: 'select',
        label: 'Outlined',
        value: '--outline'
      },
      {
        type: 'select',
        label: 'Striped',
        value: '--striped'
      }
    ],
    utils: [
      { 'Font Size': kernl.utilities.fontSize },
      { 'Background Color': kernl.utilities.background },
      { 'Text Color': kernl.utilities.color }
    ]
  },
  masthead: {
    scoped: [
      {
        type: 'select',
        label: 'Overlay',
        value: '--overlay'
      },
      {
        type: 'select',
        label: 'Black Masthead',
        value: 'bg--black'
      },
      {
        type: 'select',
        label: 'Megamenu',
        value: '--megamenu'
      },
      {
        type: 'select',
        label: 'Utility',
        value: '+utility'
      },
      {
        type: 'select',
        label: 'Has Chevron',
        value: '+chevron'
      }
    ],
    utils: []
  }
})
